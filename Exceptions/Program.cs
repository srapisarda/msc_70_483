﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            p.ExceptionDispatchInfoMethod();



        }




        private void ExceptionDispatchInfoMethod()
        {
            ExceptionDispatchInfo exceptionDispatcher = null;

            try
            {
                SalvoException salvoEx = new SalvoException("This is a test Exception using ExceptionDispacherInfo.");
                throw salvoEx;
            }
            catch (Exception ex)
            {
                exceptionDispatcher = ExceptionDispatchInfo.Capture(ex);
            }

            if (exceptionDispatcher != null)
            {

                exceptionDispatcher.Throw();
            }

            String s = Console.ReadLine();


        }



    }
}
