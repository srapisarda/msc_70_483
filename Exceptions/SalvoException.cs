﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    [Serializable]
    public class SalvoException : Exception, ISerializable{

        public SalvoException(String message): base(message) {
            WhatIs = message;
            this.When = DateTime.Now;

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(name: WhatIs, value: When);
        }


        public DateTime When { get; set; }

        public string WhatIs { get; set; }
    }
}
